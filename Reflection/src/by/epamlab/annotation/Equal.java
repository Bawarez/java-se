package by.epamlab.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation is used to mark fields to be included in the process of objects comparing.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Equal {
    /**
     * Determines a field comparision method.
     * @return
     *          Comparision.REFERENCE to determine objects equality by reference
     *          Comparision.VALUE to determine objects equality by value (state)
     */
    Comparision compareBy();
}
