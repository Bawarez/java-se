package by.epamlab.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation may appear on classes and declares that a holder class can become proxy.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Proxy {
    /**
     * a class name for using as handler
     * @return handler class name
     */
    String invocationHandler();
}
