package by.epamlab.annotation;

/**
 * Represents objects comparision methods
 */
public enum Comparision {
    /**
     * Objects comparision by reference
     */
    REFERENCE {
        @Override
        public boolean compare(final Object o1, final Object o2) {
            return o1 == o2;
        }
    },
    /**
     * Objects comparision by state
     */
    VALUE {
        @Override
        public boolean compare(final Object o1, final Object o2) {
            if (o1 == null) {
                return o2 == null;
            }
            return o1.equals(o2);
        }
    };

    /**
     * Determines if objects are equal
     * @param o1 first compared object
     * @param o2 second compared object
     * @return true if objects are equal, false - if not
     */
    public abstract boolean compare(Object o1, Object o2);
}
