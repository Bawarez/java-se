package by.epamlab.util;

import by.epamlab.annotation.Equal;

import java.lang.reflect.Field;

/**
 * An unified objects equality analyzer The utility class gets two objects
 * and makes full fields comparison. The result of the class running is
 * objects are equal or not.
 */
public final class EqualityAnalyzer {
    private EqualityAnalyzer() { }

    /**
     * Makes fields of passed objects scanning.
     * Uses an @Equal annotation to make a decision of including a field in the result.
     * @param o1 first compared object
     * @param o2 second compared object
     * @return true if all fields marked by @Equal annotation are equal, false if some of them - not
     */
    public static boolean equalObjects(final Object o1, final Object o2) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        if (o1.getClass() != o2.getClass()) {
            return false;
        }

        for (Field field : o1.getClass().getDeclaredFields()) {
            Equal equal = field.getAnnotation(Equal.class);
            if (equal != null) {
                boolean isAccessible = field.isAccessible();
                try {
                    field.setAccessible(true);
                    if (!equal.compareBy().compare(field.get(o1), field.get(o2))) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Failed to compare objects " + o1 + " " + o2, e);
                } finally {
                    field.setAccessible(isAccessible);
                }
            }
        }
       return true;
    }
}
