package by.epamlab.factory;

import by.epamlab.annotation.Proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

/**
 * The class is used to get an object instance for a class provided as attribute.
 */
public final class Factory {
    private Factory() { }

    /**
     * Creates an object for the specified class. If the class has the @Proxy annotation applied,
     * then a proxy object will be created instead of the common instance.
     * Specified class should have public default constructor.
     * @param clazz Class object representing the desired type
     * @return created instance of the specified class or a proxy object if the @Proxy annotation applied
     */
    public static Object getInstanceOf(final Class<?> clazz) {
        try {
            Object instance = clazz.getConstructor().newInstance();
            Proxy proxyAnnotation = clazz.getAnnotation(Proxy.class);

            if (proxyAnnotation != null) {
                String handlerName = proxyAnnotation.invocationHandler();
                InvocationHandler handler = (InvocationHandler) Class.forName(handlerName)
                        .getConstructor(Object.class)
                        .newInstance(instance);
                instance = java.lang.reflect.Proxy
                        .newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), handler);
            }

            return instance;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                | NoSuchMethodException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
