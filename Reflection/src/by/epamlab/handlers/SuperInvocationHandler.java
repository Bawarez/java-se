package by.epamlab.handlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * The class name speaks for itself.
 */
public class SuperInvocationHandler implements InvocationHandler {
    private Object instance;

    public SuperInvocationHandler(final Object instance) {
        this.instance = instance;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        System.out.println("SuperInvocationHandler watches you!");
        return method.invoke(instance, args);
    }
}
