package by.epamlab.beans;

import by.epamlab.annotation.Comparision;
import by.epamlab.annotation.Equal;
import by.epamlab.annotation.Proxy;

/**
 * Every application needs this class.
 * If your application doesn't need it, you you're doing something wrong.
 */
@Proxy(invocationHandler = "by.epamlab.handlers.SuperInvocationHandler")
public class VeryUsefulClass implements UsefulInterface {

    @Equal(compareBy = Comparision.REFERENCE)
    private String veryUsefulString;

    @Equal(compareBy = Comparision.REFERENCE)
    private Integer veryUsefulNumber;

    private Double veryUselessNumber;

    /**
     * Default very useful constructor
     */
    public VeryUsefulClass() {
        this.veryUsefulString = "I am very useful!";
    }

    /**
     * Very useful constructor
     * @param veryUsefulString a very useful message
     * @param veryUsefulNumber a very useful number
     * @param veryUselessNumber a very useless part of a very useless class
     */
    public VeryUsefulClass(final String veryUsefulString,
                           final Integer veryUsefulNumber,
                           final Double veryUselessNumber) {
        this.veryUsefulString = veryUsefulString;
        this.veryUsefulNumber = veryUsefulNumber;
        this.veryUselessNumber = veryUselessNumber;
    }
    @Override
    public void doSomethingUseful() {
        System.out.println(veryUsefulString);
    }
}
