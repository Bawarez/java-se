package by.epamlab.beans;

import by.epamlab.annotation.Comparision;
import by.epamlab.annotation.Equal;

/**
 * Almost every application needs this class
 */
public class UsefulClass implements UsefulInterface {

    @Equal(compareBy = Comparision.VALUE)
    private String usefulString;

    private Double uselessNumber;

    @Equal(compareBy = Comparision.VALUE)
    private Integer usefulNumber;

    /**
     * Default useful constructor
     */
    public UsefulClass() {
        this.usefulString = "I am useful!";
    }

    /**
     * Useful constructor
     * @param usefulString an useful message
     * @param usefulNumber an useful number
     * @param uselessNumber an useless part of an useful object
     */
    public UsefulClass(final String usefulString, final Integer usefulNumber, final Double uselessNumber) {
        this.usefulString = usefulString;
        this.usefulNumber = usefulNumber;
        this.uselessNumber = uselessNumber;
    }

    @Override
    public void doSomethingUseful() {
        System.out.println(usefulString);
    }
}
