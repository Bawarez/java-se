package by.epamlab.beans;

import by.epamlab.annotation.Comparision;
import by.epamlab.annotation.Equal;

/**
 * An example of useless class. Look! It doesn't implement UsefulInterface.
 * Who might need such a class?
 */
public class UselessClass {

    @Equal(compareBy = Comparision.VALUE)
    private String uselessString = "I am useless";

    @Equal(compareBy = Comparision.REFERENCE)
    private String oneMoreUselessString = new String("I can do nothing!");

    /**
     * useless constructor
     * @param uselessString useless message
     * @param oneMoreUselessString one more useless message
     */
    public UselessClass(final String uselessString, final String oneMoreUselessString) {
        this.uselessString = uselessString;
        this.oneMoreUselessString = oneMoreUselessString;
    }
}
