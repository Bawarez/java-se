package by.epamlab.beans;

/**
 * An interface that must be implemented by every every doing something useful Java class.
 * All other Java classes are useless!
 */
public interface UsefulInterface {
    /**
     * All useful Java code should be contained here.
     */
    void doSomethingUseful();
}
