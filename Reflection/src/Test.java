import by.epamlab.beans.UsefulClass;
import by.epamlab.beans.UsefulInterface;
import by.epamlab.beans.UselessClass;
import by.epamlab.beans.VeryUsefulClass;
import by.epamlab.factory.Factory;
import by.epamlab.util.EqualityAnalyzer;

/**
 * Class for testing
 */
public final class Test {
    private Test() {
    }

    /**
     * runs test
     *
     * @param args command line parameters
     */
    public static void main(final String[] args) {

        UsefulInterface useful = (UsefulInterface) Factory.getInstanceOf(UsefulClass.class);
        useful.doSomethingUseful();

        useful = (UsefulInterface) Factory.getInstanceOf(VeryUsefulClass.class);
        useful.doSomethingUseful();

        Integer integer1 = 410;
        Integer integer2 = 410;
        Integer integer3 = 400;
        Double dbl1 = 345.1;
        Double dbl2 = 345.0;
        UsefulInterface useful1 = new UsefulClass("I am useful", integer1, dbl1);
        UsefulInterface useful2 = new UsefulClass("I am useful", integer2, dbl2);
        UsefulInterface useful3 = new UsefulClass("I am useful", integer3, dbl1);
        UsefulInterface veryUseful1 = new VeryUsefulClass("I am very useful", integer1, dbl1);
        UsefulInterface veryUseful2 = new VeryUsefulClass("I am very useful", integer1, dbl2);
        UsefulInterface veryUseful3 = new VeryUsefulClass("I am very useful", integer2, dbl1);
        UselessClass useless1 = new UselessClass("I am useless", "-_^");
        UselessClass useless2 = new UselessClass(new String("I am useless"), "-_^");
        UselessClass useless3 = new UselessClass("I am useless", new String("-_^"));

        test(useful1, useful2, useful3, "value");
        test(veryUseful1, veryUseful2, veryUseful3, "reference");
        test(useless1, useless2, useless3, "twisted");
    }

    private static void test(final Object o1, final Object o2, final Object o3, final String compareBy) {
        assert EqualityAnalyzer.equalObjects(o1, o2)
                : "Comparision by " + compareBy + " test for equal objects failed";
        assert !EqualityAnalyzer.equalObjects(o1, o3)
                : "Comparision by " + compareBy + " test for not equal objects failed";
    }
}
