package validation;

import org.apache.tools.ant.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Validator of build files.
 * Depends on checked flags, checks the specified ant build files:
 * checkDepends - if targets with depends are used instead of "main" point;
 * checkDefault - if project contains default attribute;
 * checkNames - if target names contains only letters with '-'
 * @author Dzmitry Ryzhyk
 */

public class BuildFileValidator extends Task {
    private static final String NAME_REGEX = "[\\p{L}-]*";
    private static final Pattern NAME_PATTERN = Pattern.compile(NAME_REGEX);
    private boolean checkDepends;
    private boolean checkDefault;
    private boolean checkNames;
    private List<BuildFile> buildFiles = new ArrayList<>();

    @Override
    public void execute() {
        Project validatorProj = getProject();
        boolean areFilesValid = true;

        for (BuildFile bf : buildFiles) {
            Project project = createProject(bf);

            if (checkDefault) {
                areFilesValid = checkDefault(project);
            }
            if (checkDepends) {
                String issueMsg = "non-main target has dependencies";
                areFilesValid &= checkTargets(project,
                        t -> (!"main".equals(t.getName())) && (t.getDependencies().hasMoreElements()),
                        t -> validatorProj.log(t.getLocation() + issueMsg));
            }
            if (checkNames) {
                String issueMsg = "invalid name: ";
                areFilesValid &= checkTargets(project,
                        t -> !NAME_PATTERN.matcher(t.getName()).matches(),
                        t -> validatorProj.log(t.getLocation() + issueMsg + t.getName()));
            }
        }

        if (!areFilesValid) {
            throw new BuildException("One or more build files are invalid");
        }
    }

    private boolean checkDefault(final Project project) {
        boolean isDefaultSet = project.getDefaultTarget() != null;

        if (!isDefaultSet) {
            getProject().log("Default target is not specified in project " + project.getName());
        }
        return isDefaultSet;
    }

    private boolean checkTargets(final Project project,
                                 final Predicate<Target> invalidTargetFinder,
                                 final Consumer<Target> invalidTargetHandler) {
        return project.getTargets().values().stream()
                .filter(invalidTargetFinder)
                .peek(invalidTargetHandler)
                .collect(Collectors.toList()).isEmpty();
    }

    private Project createProject(final BuildFile bf) {
        File file = new File(bf.location);
        Project project = new Project();
        project.init();
        ProjectHelper.configureProject(project, file);

        return project;
    }

    /**
     * checkDepends setter
     * @param checkDepends
     *        attribute value
     */
    public void setCheckDepends(final boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * checkDefault setter
     * @param checkDefault
     *        attribute value
     */
    public void setCheckDefault(final boolean checkDefault) {
        this.checkDefault = checkDefault;
    }

    /**
     * checkNames setter
     * @param checkNames
     *        attribute value
     */
    public void setCheckNames(final boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * method creates a new instence of BildFile and adds it to buildfileList
     * @return created instance of BuildFile
     */
    public BuildFile createBuildFile() {
        BuildFile bf = new BuildFile();
        buildFiles.add(bf);
        return bf;
    }

    /**
     * Inner class that represents path to file to be validated
     */
    public class BuildFile {
        private String location;

        /**
         * location setter
         * @param location
         *        path to file
         */
        public void setLocation(final String location) {
            this.location = location;
        }
    }
}
