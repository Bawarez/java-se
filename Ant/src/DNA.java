/**
 * Class draws two sin waves
 * @author Dzmitry Ryzhyk
 */


public final class DNA {
    private static final int LENGTH = 40;
    private static final int DELAY = 50;
    private static final int FRAGMENTATION = 17;
    private static final int WAVES_NUMBER = 50;
    private static final char DEFAULT_CHARACTER = '@';
	

    private DNA() {
    }

    /**
     * Main method
     * @param args
     *        a character to draw the waves
     *        if program is run without arguments, default character will be used
     */
    public static void main(final String[] args) {
        char character = ((args.length == 1) && (!args[0].isEmpty())) ? args[0].charAt(0)
                                                                      : DEFAULT_CHARACTER;

        double i = Math.PI;

        while (i <= Math.PI * WAVES_NUMBER) {
            int firstSymbolPos = (int) (Math.cos(i) * LENGTH);
            int secondSymbolPos = (int) (Math.sin(i - Math.PI / 2) * LENGTH);
            for (int j = -LENGTH; j <= LENGTH; j++) {
                if ((j == firstSymbolPos) || (j == secondSymbolPos)) {
                    System.out.print(character);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();

            i += Math.PI / FRAGMENTATION;
            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                System.out.println("Something went wrong :(");
                break;
            }
        }
    }
}
