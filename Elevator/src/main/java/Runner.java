import com.epamlab.beans.Building;
import com.epamlab.factories.BuildingBuilder;
import com.epamlab.service.ConfigReader;
import com.epamlab.service.Controller;

/**
 * Elevator runner
 */
public final class Runner {
    private static final String CONFIG_FILE_PATH = "config";
    private static final String PROP_FLOORS_NUMBER = "floorsNumber";
    private static final String PROP_ELEVATOR_CAPACITY = "elevatorCapacity";
    private static final String PROP_PASSENGERS_NUMBER = "passengersNumber";
    private static int floorsNumber;
    private static int elevatorCapacity;
    private static int passengersNumber;

    private Runner() { }

    /**
     * main method
     * @param args command line parameters
     */
    public static void main(final String[] args) {
        configure();

        Building building = new BuildingBuilder(floorsNumber)
                .createElevator(elevatorCapacity)
                .fillWithPassengers(passengersNumber)
                .build();

        Controller controller = new Controller(building);
        controller.start();
    }

    private static void configure() {
        ConfigReader configReader = new ConfigReader(CONFIG_FILE_PATH);
        floorsNumber = configReader.getInt(PROP_FLOORS_NUMBER);
        elevatorCapacity = configReader.getInt(PROP_ELEVATOR_CAPACITY);
        passengersNumber = configReader.getInt(PROP_PASSENGERS_NUMBER);
    }
}
