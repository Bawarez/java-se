package com.epamlab.container;

import com.epamlab.enums.Direction;
import com.epamlab.beans.Passenger;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Represents the elevator transporting passengers
 */
public class Elevator {
    private Direction direction = Direction.UP;
    private int capacity;
    private ListIterator<Floor> floors;
    private List<Passenger> passengers;


    /**
     * Constructs an elevator that can accommodate a certain number of passengers
     * @param capacity elevator capacity that must be positive
     * @param floors floor iterator
     * @throws IllegalArgumentException if capacity < 1
     */
    public Elevator(final int capacity, final ListIterator<Floor> floors) {
        if (capacity < 1) {
            throw new IllegalArgumentException("Non-positive capacity: " + capacity);
        }
        this.capacity = capacity;
        this.floors = floors;
        this.passengers = new ArrayList<>(capacity);
    }

    /**
     * Moves this elevator to the next floor in current direction
     * @return next floor
     */
    public Floor nextFloor() {
        Floor floor;
        if (direction == Direction.UP) {
            floor = floors.next();
            if (!floors.hasNext()) {
                direction = Direction.DOWN;
                floors.previous();
            }
        } else {
            floor = floors.previous();
            if (!floors.hasPrevious()) {
                direction = Direction.UP;
                floors.next();
            }
        }
        return floor;
    }

    /**
     * Adds the specified passenger into the elevator
     * @param passenger passenger to add
     * @throws IllegalStateException if the elevator passengers number == capacity when method called
     */
    public void addPassenger(final Passenger passenger) {
        if (isFull()) {
            throw new IllegalStateException("The elevator is full");
        }
        passengers.add(passenger);
    }

    /**
     * Debarks passenger specified passenger from this elevator
     * @param passenger passenger to debark
     * @throws NoSuchElementException if the passenger absents in the elevator
     */
    public void debark(final Passenger passenger) {
        if (!passengers.remove(passenger)) {
            throw new NoSuchElementException("There is no passenger " + passenger.getId() + " in the elevator");
        }
    }

    /**
     * Returns true if there is some passengers to arrive to the floor with specified number
     * @param floor destination floor number
     * @return true if there is some passengers to arrive to the floor with specified number
     */
    public boolean hasPassengersToLeave(final int floor) {
        return passengers.stream()
                .anyMatch(p -> p.getDestination() == floor);
    }

    /**
     * returns true if this elevator is full, false otherwise
     * @return true if this elevator is full
     */
    public boolean isFull() {
        return passengers.size() == capacity;
    }

    /**
     * Returns this elevator capacity
     * @return elevator capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Returns current direction
     * @return current direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Returns passengers list
     * @return passengers list
     */
    public List<Passenger> getPassengers() {
        return passengers;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Passengers in the elevator:\n");
        for (Passenger passenger : passengers) {
            sb.append(passenger)
                    .append("\n");
        }
        return  sb.toString();
    }
}
