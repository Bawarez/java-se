package com.epamlab.container;

import com.epamlab.beans.Passenger;
import com.epamlab.enums.Direction;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Represents a building floor
 */
public class Floor {
    private List<Passenger> dispatchFloorContainer = new CopyOnWriteArrayList<>();
    private List<Passenger> arrivalFloorContainer = new CopyOnWriteArrayList<>();
    private int number;

    /**
     * constructs a floor
     * @param number floor number
     */
    public Floor(final int number) {
        this.number = number;
    }



    /**
     * places the specified passenger passenger to the arrivalFloorContainer
     * @param passenger passenger arriving to the floor
     */
    public void arrive(final Passenger passenger) {
        arrivalFloorContainer.add(passenger);
    }

    /**
     * places the specified passenger passenger to the dispatchFloorContainer
     * @param passenger passenger living the floor
     */
    public void dispatch(final Passenger passenger) {
        int destination = passenger.getDestination();
        if (destination == number) {
            throw new IllegalArgumentException("The passenger's destination is the current floor "
                    + passenger);
        }
        dispatchFloorContainer.add(passenger);
    }

    /**
     * removes the specified passenger from the dispatchFloorContainer
     * @param passenger passenger to remove
     */
    public void leaveDispatchZone(final Passenger passenger) {
        if (!dispatchFloorContainer.remove(passenger)) {
            throw new NoSuchElementException("There is no passenger" + passenger.getId() + " on the floor " + number);
        }
    }

    /**
     * Returns true if there are some passengers on the floor to leave the dispatch zone
     * to transport in the specified direction
     * @param direction transportation direction
     * @return true if there are some passengers on the floor to leave the dispatch zone
     *         to transport in the specified direction
     */
    public boolean hasPassengersToLeave(final Direction direction) {
        return dispatchFloorContainer.stream()
                .anyMatch(p -> direction.matches(p.getInitialFloor().getNumber(), p.getDestination()));
    }

    /**
     * returns passengers waiting for on the floor
     * @return dispatchFloorContainer passengers number
     */
    public List<Passenger> waitingPassengers() {
        return new ArrayList<>(dispatchFloorContainer);
    }

    /**
     * returns passengers arrived on the floor
     * @return arrivalFloorContainer passengers number
     */
    public List<Passenger> arrivedPassengers() {
        return new ArrayList<>(arrivalFloorContainer);
    }

    /**
     * returns this floor number
     * @return floor number
     */
    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Floor ")
                .append(number)
                .append("\nDispatch floor container:\n")
                .append(containerToString(dispatchFloorContainer))
                .append("Arrival floor container:\n")
                .append(containerToString(arrivalFloorContainer));
        return sb.toString();
    }

    private String containerToString(final List<Passenger> passengers) {
        StringBuilder sb = new StringBuilder();
        for (Passenger passenger : passengers) {
            sb.append(passenger)
                    .append("\n");
        }
        return sb.toString();
    }
}
