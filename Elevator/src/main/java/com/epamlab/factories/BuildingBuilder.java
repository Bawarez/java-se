package com.epamlab.factories;

import com.epamlab.beans.Passenger;
import com.epamlab.beans.Building;
import com.epamlab.container.Elevator;
import com.epamlab.container.Floor;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Building builder
 */
public class BuildingBuilder {
    private List<Floor> floors;
    private Elevator elevator;

    /**
     * Constructs a builder with specified floors number
     * @param floorsNumber floors number
     */
    public BuildingBuilder(final int floorsNumber) {
        if (floorsNumber < 1) {
            throw new IllegalArgumentException("Wrong floors number: " + floorsNumber);
        }
        Floor[] floorsArray = new Floor[floorsNumber];
        for (int i = 0; i < floorsNumber; i++) {
            floorsArray[i] = new Floor(i + 1);
        }
        floors = Arrays.asList(floorsArray);
    }

    /**
     * Randomly fills dispatch floor containers with specified number of passengers
     * @param passengersNumber passengers number
     * @return this builder
     */
    public BuildingBuilder fillWithPassengers(final int passengersNumber) {
        Random rnd = new Random();
        int floorsNumber = floors.size();

        for (int i = 0; i < passengersNumber; i++) {
            int destinationFloor = rnd.nextInt(floorsNumber);
            int initialFloor = rnd.nextInt(floorsNumber);
            if (initialFloor == destinationFloor) {
                initialFloor = (initialFloor + floorsNumber / 2) % floorsNumber;
            }

            Floor floor = floors.get(initialFloor);
            floor.dispatch(new Passenger(floor, destinationFloor + 1));
        }
        return this;
    }

    /**
     * Creates and sets a new elevator with specified capacity
     * @param capacity elevator capacity
     * @return this builder
     */
    public BuildingBuilder createElevator(final int capacity) {
        this.elevator = new Elevator(capacity, floors.listIterator());
        return this;
    }

    /**
     * Creates a building
     * @return new building
     */
    public Building build() {
        return new Building(floors, elevator);
    }
}
