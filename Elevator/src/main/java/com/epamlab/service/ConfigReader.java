package com.epamlab.service;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Reads configuration
 */
public class ConfigReader {
    private ResourceBundle rb;

    /**
     * Constructs an instance reading properties from specified file
     * @param source path to the config file
     */
    public ConfigReader(final String source) {
        rb = ResourceBundle.getBundle(source);
    }

    /**
     * Gets a int number for the given key.
     *
     * @param key the key for the desired string
     * @exception MissingResourceException if no object for the given key can be found
     * @exception NumberFormatException if the value does not contain parsable integer
     * @return the string for the given key
     */
    public int getInt(final String key) {
        return Integer.parseInt(rb.getString(key));
    }

    /**
     * Gets a string for the given key.
     *
     * @param key the key for the desired string
     * @exception NullPointerException if key is null
     * @exception MissingResourceException if no object for the given key can be found
     * @exception ClassCastException if the object found for the given key is not a string
     * @return the string for the given key
     */
    public  String getString(final String key) {
        return rb.getString(key);
    }
}
