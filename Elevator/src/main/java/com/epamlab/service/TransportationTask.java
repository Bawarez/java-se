package com.epamlab.service;

import com.epamlab.beans.Passenger;
import com.epamlab.container.Elevator;
import com.epamlab.container.Floor;
import com.epamlab.enums.Direction;
import com.epamlab.enums.TransportationState;
import org.apache.log4j.Logger;

/**
 * Manages passenger transportation process
 */
public class TransportationTask extends Thread {
    private static final Logger LOGGER = Logger.getLogger(TransportationTask.class);
    private final Passenger passenger;
    private final Controller controller;
    private final Direction direction;
    private boolean isInElevator = false;

    /**
     * Constructs an enums task serves the specified passenger
     * @param passenger passenger to be served
     * @param controller elevator controller
     */
    public TransportationTask(final Passenger passenger, final Controller controller) {
        super("TransportationTask " + passenger.getId());
        this.passenger = passenger;
        this.controller = controller;

        int initFloor = passenger.getInitialFloor().getNumber();
        this.direction = (initFloor < passenger.getDestination()) ? Direction.UP
                                                                  : Direction.DOWN;
    }

    @Override
    public void run() {
        passenger.setTransportationState(TransportationState.IN_PROGRESS);
        enterTheElevator();
        arriveToDestinationFloor();
    }

    private void enterTheElevator() {
        Floor initialFloor = passenger.getInitialFloor();
        while (!isInElevator) {
            while (controller.getFloorNumber() != initialFloor.getNumber()
                    || controller.getDirection() != direction) {
                synchronized (initialFloor) {
                    try {
                        initialFloor.wait();
                    } catch (InterruptedException e) {
                        LOGGER.error(e);
                    }
                }
            }
            isInElevator = controller.accept(passenger);
        }
    }

    private void arriveToDestinationFloor() {
        Elevator elevator = controller.getElevator();
        while (controller.getFloorNumber() != passenger.getDestination()) {
            synchronized (elevator) {
                try {
                    elevator.wait();
                } catch (InterruptedException e) {
                    LOGGER.error(e);
                }
            }
        }
        controller.debark(passenger);
        passenger.setTransportationState(TransportationState.COMPLETED);
    }
}
