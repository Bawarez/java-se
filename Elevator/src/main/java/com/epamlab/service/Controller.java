package com.epamlab.service;

import com.epamlab.beans.Building;
import com.epamlab.container.Elevator;
import com.epamlab.container.Floor;
import com.epamlab.enums.Direction;
import com.epamlab.beans.Passenger;
import com.epamlab.enums.TransportationState;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Elevator controller
 */
public class Controller {
    private static final Logger LOGGER = Logger.getLogger(Controller.class);
    private final ReentrantLock lock = new ReentrantLock(true);
    private final Condition isDebarked = lock.newCondition();
    private final Condition isAccepted = lock.newCondition();
    private final Building building;
    private final Elevator elevator;
    private volatile Floor currentFloor;
    private ExecutorService executorService;

    /**
     * Constructs a controller instance working with the specified elevator
     * @param building elevator to be managed
     */
    public Controller(final Building building) {
        Elevator elevator = building.getElevator();
        if (elevator == null) {
            throw new IllegalStateException("The building doesn't have any elevator");
        }
        this.building = building;
        this.elevator = elevator;
    }

    /**
     * Starts transportation process
     */
    public void start() {
        createTasks();
        while (!allPassengersArrived()) {
            nextFloor();
        }
        executorService.shutdown();
        logResultState();
    }

    private boolean allPassengersArrived() {
        List<Passenger> passengers = building.waitingPassengers();
        for (Passenger passenger : passengers) {
            if (passenger.getTransportationState() != TransportationState.COMPLETED) {
                return false;
            }
        }
        return (elevator.getPassengers().size() == 0);
    }

    private void nextFloor() {
        currentFloor = elevator.nextFloor();
        LOGGER.info(currentFloor);

        debarkPassengers();
        acceptPassengers();

        LOGGER.info(currentFloor);
        LOGGER.info(elevator);
    }

    private void debarkPassengers() {
        synchronized (elevator) {
            elevator.notifyAll();
        }
        lock.lock();
        try {
            while (elevator.hasPassengersToLeave(currentFloor.getNumber())) {
                isDebarked.await();
            }
        } catch (InterruptedException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
    }

    private void acceptPassengers() {
        synchronized (currentFloor) {
            currentFloor.notifyAll();
        }
        lock.lock();
        try {

            while (!isLandingOver()) {
                    isAccepted.await();
            }
        }  catch (InterruptedException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
    }

    private boolean isLandingOver() {
        return elevator.isFull()
                || !currentFloor.hasPassengersToLeave(elevator.getDirection());
    }

    private void logResultState() {
        LOGGER.info("Transportation finished");
        for (Floor floor : building.getFloors()) {
            LOGGER.info(floor);
        }
        LOGGER.info(elevator);
    }

    /**
     * Accepts the specified passenger into the elevator if there is enough place
     * @param passenger passenger
     * @return whether passenger was accepted
     */
    public boolean accept(final Passenger passenger) {
        lock.lock();
        try {
            if (elevator.isFull()) {
                return false;
            }
            passenger.getInitialFloor().leaveDispatchZone(passenger);
            elevator.addPassenger(passenger);
            LOGGER.info("Passenger " + passenger.getId() + " accepted");
            isAccepted.signal();
            return true;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Debarks the specified passenger from the elevator
     * @param passenger passenger to be debark
     */
    public void debark(final Passenger passenger) {
        lock.lock();
        try {
            elevator.debark(passenger);
            currentFloor.arrive(passenger);
            LOGGER.info("Passenger " + passenger.getId() + " debarked");
            isDebarked.signal();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns current floor number
     * @return current floor number
     */
    public int getFloorNumber() {
        if (currentFloor == null) {
            return 0;
        }
        return currentFloor.getNumber();
    }

    /**
     * Returns the elevator direction
     * @return the elevators direction
     */
    public Direction getDirection() {
        return elevator.getDirection();
    }

    /**
     * returns he elevator maintained by this controller
     * @return the elevator maintained by this controller
     */
    public Elevator getElevator() {
        return elevator;
    }

    private void createTasks() {
        List<Passenger> passengers = building.waitingPassengers();
        executorService = Executors.newFixedThreadPool(passengers.size());

        for (Passenger passenger : passengers) {
            executorService.execute(new TransportationTask(passenger, this));
        }
    }
}
