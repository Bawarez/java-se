package com.epamlab.enums;

/**
 * Transportation direction
 */
public enum Direction {
    /**
     * up direction
     */
    UP {
        @Override
        public boolean matches(final int from, final int to) {
            return from < to;
        }
    },
    /**
     * down direction
     */
    DOWN {
        @Override
        public boolean matches(final int from, final int to) {
            return from > to;
        }
    };

    /**
     * returns a boolean value that means whether the direction matches
     * with direction represented by specified departure and destination floors
     * @param from departure floor number
     * @param to destination floor number
     * @return whether the direction matches
     */
    public abstract boolean matches(int from, int to);
}
