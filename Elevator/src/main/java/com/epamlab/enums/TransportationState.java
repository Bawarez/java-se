package com.epamlab.enums;

/**
 * Represents passengers transportation state
 */
public enum TransportationState {
    /**
     * Transportation not started
     */
    NOT_STARTED,
    /**
     * Transportation in progress
     */
    IN_PROGRESS,
    /**
     * Transportation in progress
     */
    COMPLETED
}
