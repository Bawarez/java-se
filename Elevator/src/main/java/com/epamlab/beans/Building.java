package com.epamlab.beans;

import com.epamlab.container.Elevator;
import com.epamlab.container.Floor;

import java.util.*;

/**
 * Represents a building with fixed floors number
 */
public class Building {
    private List<Floor> floors;
    private Elevator elevator;

    /**
     * Constructs a building with specified floors and elevator
     * @param floors floors
     * @param elevator elevator
     */
     public Building(final List<Floor> floors, final Elevator elevator) {
        this.floors = floors;
        this.elevator = elevator;
     }

    /**
     * Returns list of passengers allocated in this building
     * @return list of passengers allocated in this building
     */
    public List<Passenger> waitingPassengers() {
        List<Passenger> passengers = new ArrayList<>();
        for (Floor floor: floors) {
            passengers.addAll(floor.waitingPassengers());
        }
        return passengers;
    }

    /**
     * Returns list of passengers allocated in this building
     * @return list of passengers allocated in this building
     */
    public List<Passenger> arrivedPassengers() {
        List<Passenger> passengers = new ArrayList<>();
        for (Floor floor: floors) {
            passengers.addAll(floor.arrivedPassengers());
        }
        return passengers;
    }

    /**
     * Returns elevator
     * @return elevator or null if elevator isn't set
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * Returns floors list
     * @return floors list
     */
    public List<Floor> getFloors() {
        return floors;
    }

    /**
     * Sets the specified elevator
     * @param elevator elevator to set
     */
    public void setElevator(final Elevator elevator) {
        this.elevator = elevator;
    }
}
