package com.epamlab.beans;

import com.epamlab.container.Floor;
import com.epamlab.enums.TransportationState;

/**
 * Represents a passenger that wants to reach some floor
 */
public class Passenger {
    private static long idCounter = 0;
    private final long id;
    private final int destination;
    private final Floor initialFloor;
    private TransportationState transportationState = TransportationState.NOT_STARTED;

    /**
     * Constructs a passenger
     * @param initialFloor initial floor
     * @param destination destination floor number
     */
    public Passenger(final Floor initialFloor, final int destination) {
        if (initialFloor == null) {
            throw new IllegalArgumentException("initial floor is null");
        }
        this.initialFloor = initialFloor;
        this.destination = destination;
        this.id = idCounter++;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Passenger)) {
            return false;
        }

        Passenger other = (Passenger) o;
        return id == other.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Passenger " + id + ", destination " + destination + ", "
                + transportationState;
    }

    /**
     * Gets passenger's id
     * @return passenger's id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets passenger's destination floor number
     * @return destination floor number
     */
    public int getDestination() {
        return destination;
    }

    /**
     * Gets passenger's initial floor number
     * @return initial floor number
     */
    public Floor getInitialFloor() {
        return initialFloor;
    }

    /**
     * Gets passenger's enums state
     * @return enums state
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }

    /**
     * Sets transpotrstion state for this passenger
     * @param transportationState enums state to set
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }
}
