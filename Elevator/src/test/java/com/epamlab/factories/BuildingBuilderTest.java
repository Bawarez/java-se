package com.epamlab.factories;

import com.epamlab.beans.Building;
import com.epamlab.enums.TransportationState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BuildingBuilderTest {
    private BuildingBuilder buildingBuilder;
    @Before
    public void init() {
        buildingBuilder = new BuildingBuilder(10);
    }

    @After
    public void destroy() {
        buildingBuilder = null;
    }

    @Test
    public void buildFloorsNumberEquals() {
        Building building = buildingBuilder.build();
        int floorsNumber = building.getFloors().size();
        assertEquals(10, floorsNumber);
    }

    @Test
    public void buildFloorsNumberNotEquals() {
        Building building = buildingBuilder.build();
        int floorsNumber = building.getFloors().size();
        assertNotEquals(9, floorsNumber);
    }

    @Test
    public void fillWithPassengersNumberEquals() {
        Building building = buildingBuilder.fillWithPassengers(410).build();
        int passengersNumber = building.getFloors().stream()
                .mapToInt(f -> f.waitingPassengers().size())
                .sum();
        assertEquals(410, passengersNumber);
    }

    @Test
    public void fillWithPassengersNumberNotEquals() {
        Building building = buildingBuilder.fillWithPassengers(55).build();
        int passengersNumber = building.getFloors().stream()
                .mapToInt(f -> f.waitingPassengers().size())
                .sum();
        assertNotEquals(410, passengersNumber);
    }

    @Test
    public void allPassengersNotStarted() {
        Building building = buildingBuilder.fillWithPassengers(55).build();
        building.getFloors().forEach(f -> f.waitingPassengers()
                .forEach(p -> assertEquals(TransportationState.NOT_STARTED, p.getTransportationState())));
    }

    @Test
    public void intiAndDestinationFloorsNotEqual() {
        Building building = buildingBuilder.fillWithPassengers(32).build();
        building.waitingPassengers().forEach(p -> assertNotEquals(p.getInitialFloor().getNumber(), p.getDestination()));
    }

    @Test
    public void createElevatorCapacityEquals() {
        Building building = buildingBuilder.createElevator(3).build();
        int elevatorCapacity = building.getElevator().getCapacity();
        assertEquals(3, elevatorCapacity);
    }

    @Test
    public void createElevatorCapacityNotEquals() {
        Building building = buildingBuilder.createElevator(5).build();
        int elevatorCapacity = building.getElevator().getCapacity();
        assertNotEquals(3, elevatorCapacity);
    }

    @Test
    public void createElevatorNotNull() {
        Building building = buildingBuilder.createElevator(3).build();
        assertNotNull(building.getElevator());
    }

    @Test
    public void buildNotNull() {
        Building building = buildingBuilder.build();
        assertNotNull(building);
    }
}