package com.epamlab.container;

import com.epamlab.beans.Building;
import com.epamlab.beans.Passenger;
import com.epamlab.enums.Direction;
import com.epamlab.factories.BuildingBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ElevatorTest {
    private Building building;
    private Elevator elevator;

    @Before
    public void setUp() {
        building = new BuildingBuilder(5)
                .createElevator(3)
                .build();
        elevator = building.getElevator();
    }

    @After
    public void tearDown() {
        building = null;
        elevator = null;
    }

    @Test
    public void nextFloorNotSame() {
        Floor floor1 = elevator.nextFloor();
        Floor floor2 = elevator.nextFloor();
        assertNotSame(floor1, floor2);
    }

    @Test
    public void nextFloorNumbersDiff() {
        Floor floor1 = elevator.nextFloor();
        Floor floor2 = elevator.nextFloor();
        int diff = Math.abs(floor1.getNumber() - floor2.getNumber());
        assertEquals(1, diff);
    }

    @Test
    public void initDirectionIsUP() {
        assertEquals(Direction.UP, elevator.getDirection());
    }

    @Test
    public void TopFloorReached() {
        Floor floor = null;
        int floorsNumber = building.getFloors().size();

        for (int i = 0; i < floorsNumber; i++) {
            floor = elevator.nextFloor();
        }
        assertEquals(floorsNumber, floor.getNumber());
    }

    @Test
    public void FirstFloorReached() {
        Floor floor = null;
        int toTopFloorAndBack = building.getFloors().size() * 2 - 1;

        for (int i = 0; i < toTopFloorAndBack; i++) {
            floor = elevator.nextFloor();
        }
        assertEquals(1, floor.getNumber());
    }

    @Test
    public void ChangeDirectionAtTopFlor() {
        int toTopFloor = building.getFloors().size();
        for (int i = 0; i < toTopFloor; i++) {
            elevator.nextFloor();
        }
        assertEquals(Direction.DOWN, elevator.getDirection());
    }

    @Test
    public void ChangeDirectionAtFirstFlor() {
        int toTopFloorAndBack = building.getFloors().size() * 2 - 1;
        for (int i = 0; i < toTopFloorAndBack; i++) {
            elevator.nextFloor();
        }
        assertEquals(Direction.UP, elevator.getDirection());
    }

    @Test
    public void addPassenger() {
        Passenger passenger = new Passenger(new Floor(1), 2);
        elevator.addPassenger(passenger);
        assertTrue(elevator.getPassengers().contains(passenger));
    }

    @Test
    public void debark() {
        Passenger passenger = new Passenger(new Floor(1), 2);
        elevator.addPassenger(passenger);
        elevator.debark(passenger);
        assertFalse(elevator.getPassengers().contains(passenger));
    }

    @Test
    public void hasPassengersToLeaveTrue() {
        Passenger passenger = new Passenger(new Floor(1), 2);
        elevator.addPassenger(passenger);
        assertTrue(elevator.hasPassengersToLeave(2));
    }

    @Test
    public void hasPassengersToLeaveFalse() {
        Passenger passenger = new Passenger(new Floor(1), 2);
        elevator.addPassenger(passenger);
        assertFalse(elevator.hasPassengersToLeave(3));
    }

    @Test
    public void isFullTrue() {
        for (int i = 0; i < elevator.getCapacity(); i++) {
            Passenger passenger = new Passenger(new Floor(1), 2);
            elevator.addPassenger(passenger);
        }
        assertTrue(elevator.isFull());
    }

    @Test
    public void isFullFalse() {
        for (int i = 0; i < elevator.getCapacity() - 1; i++) {
            Passenger passenger = new Passenger(new Floor(1), 2);
            elevator.addPassenger(passenger);
        }
        assertFalse(elevator.isFull());
    }
}