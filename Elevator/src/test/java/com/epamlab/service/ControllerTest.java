package com.epamlab.service;

import com.epamlab.beans.Passenger;
import com.epamlab.beans.Building;
import com.epamlab.container.Floor;
import com.epamlab.enums.TransportationState;
import com.epamlab.factories.BuildingBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class ControllerTest {
    private Building building;
    private Controller controller;

    @Before
    public void init() {
        building = new BuildingBuilder(10)
                .createElevator(5)
                .fillWithPassengers(37)
                .build();
        controller = new Controller(building);
    }

    @After
    public void destroy() {
        building = null;
        controller = null;
    }

    @Test
    public void startDispatchContainersAreEmpty() {
        controller.start();
        assertTrue(building.getFloors()
                .stream()
                .allMatch(f -> f.waitingPassengers().isEmpty()));
    }

    @Test
    public void startAllPassengersArrived() {
        controller.start();
        int arrivedPassengers = building.getFloors()
                .stream()
                .mapToInt(f -> f.arrivedPassengers().size())
                .sum();
        assertEquals(37, arrivedPassengers);
    }

    @Test
    public void elevatorIsEmpty() {
        controller.start();
        int passengers = controller.getElevator()
                .getPassengers()
                .size();
        assertEquals(0, passengers);
    }

    @Test
    public void startTransportationStateCompleted() {
        controller.start();
        building.getFloors().forEach(f -> f.arrivedPassengers()
                .forEach(p -> assertEquals(TransportationState.COMPLETED, p.getTransportationState())));
    }

    @Test
    public void startTransportationStateInProgressAbsents() {
        controller.start();
        building.getFloors().forEach(f -> f.arrivedPassengers()
                .forEach(p -> assertNotEquals(TransportationState.IN_PROGRESS, p.getTransportationState())));
    }

    @Test
    public void startTransportationStateNotStartedAbsents() {
        controller.start();
        building.getFloors().forEach(f -> f.arrivedPassengers()
                .forEach(p -> assertNotEquals(TransportationState.NOT_STARTED, p.getTransportationState())));
    }


    @Test
    public void accept() {
        Passenger passenger = dispatchPassenger(410);
        controller.accept(passenger);
        boolean isPassengerOnFloor = building.getFloors().get(0)
                .waitingPassengers()
                .contains(passenger);
        boolean isPassengerInElevator = controller.getElevator()
                .getPassengers()
                .contains(passenger);
        assertFalse(isPassengerOnFloor);
        assertTrue(isPassengerInElevator);
    }

    @Test
    public void acceptElevatorNotOverloaded() {
        for (int i = 0; i < 7; i++) {
            Passenger passenger = dispatchPassenger(410);
            controller.accept(passenger);
        }
        assertEquals(5, controller.getElevator().getPassengers().size());
    }

    @Test
    public void debark() throws Exception {
        Passenger passenger = dispatchPassenger(2);
        controller.accept(passenger);

        Class<Controller> controllerClass = Controller.class;
        Field curFloor = controllerClass.getDeclaredField("currentFloor");
        try {
            curFloor.setAccessible(true);
            curFloor.set(controller, building.getFloors().get(1));
        } finally {
            curFloor.setAccessible(false);
        }

        controller.debark(passenger);
        boolean isPassengerOnFloor = building.getFloors().get(1)
                .arrivedPassengers()
                .contains(passenger);
        boolean isPassengerInElevator = controller.getElevator()
                .getPassengers()
                .contains(passenger);
        assertTrue(isPassengerOnFloor);
        assertFalse(isPassengerInElevator);
    }


    private Passenger dispatchPassenger(int destFloor) {
        Floor floor = building.getFloors().get(0);
        Passenger passenger = new Passenger(floor, destFloor);
        floor.dispatch(passenger);
        return passenger;
    }
}