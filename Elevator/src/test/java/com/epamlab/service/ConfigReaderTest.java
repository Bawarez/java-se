package com.epamlab.service;

import org.junit.Test;

import java.util.MissingResourceException;

import static org.junit.Assert.*;

public class ConfigReaderTest {
    private ConfigReader configReader = new ConfigReader("config");

    @Test
    public void getFloorsNumber() {
        int fn = configReader.getInt("floorsNumber");
        assertEquals(5, fn);
    }

    @Test
    public void getCapacity() {
        int ec = configReader.getInt("elevatorCapacity");
        assertEquals(4, ec);
    }

    @Test
    public void getPassengersNumber() {
        int pn = configReader.getInt("passengersNumber");
        assertEquals(17, pn);
    }

    @Test(expected = MissingResourceException.class)
    public void missingResource() {
        new ConfigReader("ololo");
    }
}