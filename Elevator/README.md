The elevator transports passengers from their initial floor to destination floor.
The application extracts floorsNumber, elevatorCapacity, passengersNumber from config.properties file to initialize.
floorsNumber - number of floors (positive integer number)
elevatorCapacity - maximum number of passengers an elevator can accommodate
passengerNumber - total number of passengers to be transported
When all passengers have reached the destination floor, the program ends.
Good luck!