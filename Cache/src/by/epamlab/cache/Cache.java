package by.epamlab.cache;

/**
 * Declares common methods to work with cache
 * @param <K> query type
 * @param <V> result type
 */
public interface Cache<K, V> {
    /**
     * Gets cached result by specified query
     * @param key query
     * @return query result or {@code null}, if specified query is not cached
     */
    V get(K key);

    /**
     * Caches specified query result
     * @param key query
     * @param value query result
     */
    void put(K key, V value);
}
