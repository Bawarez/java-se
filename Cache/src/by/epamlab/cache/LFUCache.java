package by.epamlab.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Least Frequency Used cache.
 * Caches some amount of query results.
 * Removes least frequency used results, if size limit is reached.
 * @param <K> query type
 * @param <V> result type
 */
public class LFUCache<K, V> implements Cache<K, V> {
    private static final int DEFAULT_SIZE = 32;
    private final Map<K, V> cache = new HashMap<>();
    private final Queue<Node> nodes = new PriorityQueue<>();
    private final int maxSize;

    /**
     * Constructs a new instance with default cache size limit.
     */
    public LFUCache() {
        this.maxSize = DEFAULT_SIZE;
    }

    /**
     * Constructs a new instance with specified cache size limit.
     * @param maxSize cache size limit must be positive.
     */
    public LFUCache(final int maxSize) {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("Illegal cache size " + maxSize);
        }
        this.maxSize = maxSize;
    }

    @Override
    public synchronized V get(final K key) {
        V value = cache.get(key);
        if (value != null) {
            nodes.stream()
                    .filter(n -> n.key.equals(key))
                    .peek(Node::increaseFrequency)
                    .findAny()
                    .ifPresent(n -> {
                        nodes.remove(n);
                        nodes.add(n);
                    });

        }
        return value;
    }

    @Override
    public synchronized void put(final K key, final V value) {
        if (key == null) {
            throw new IllegalArgumentException("null key");
        }
        if (cache.size() == maxSize) {
            Node removedNode = nodes.remove();
            cache.remove(removedNode.key);
        }
        cache.put(key, value);
        nodes.add(new Node(key));
    }

    private final class Node implements Comparable<Node> {
        private final K key;
        private int frequency;

        private Node(final K key) {
            this.key = key;
            this.frequency = 0;
        }

        void increaseFrequency() {
            if (frequency < Integer.MAX_VALUE) {
                frequency++;
            }
        }

        @Override
        public int compareTo(final Node o) {
            return frequency - o.frequency;
        }

        @Override
        public String toString() {
            return "Key: " + key + "; frequency: " + frequency;
        }
    }
}
