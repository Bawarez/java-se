package by.epamlab.cache;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Least Recently Used cache.
 * Caches some amount of query results.
 * Removes least recently used results, if size limit is reached.
 * @param <K> query type
 * @param <V> result type
 */
public class LRUCache<K, V> implements Cache<K, V> {
    private static final float LOAD_FACTOR = 0.75f;
    private static final int DEFAULT_SIZE = 16;
    private int maxSize;
    private Map<K, V> cache = Collections.synchronizedMap(new LinkedHashMap<K, V>(maxSize, LOAD_FACTOR, true) {
        @Override
        protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
            return size() > maxSize;
        }
    });

    /**
     * Constructs a new instance with default cache size limit.
     */
    public LRUCache() {
        this.maxSize = DEFAULT_SIZE;
    }

    /**
     * Constructs a new instance with specified cache size limit.
     * @param maxSize cache size limit must be positive.
     */
    public LRUCache(final int maxSize) {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("Illegal cache size " + maxSize);
        }
        this.maxSize = maxSize;
    }

    @Override
    public V get(final K key) {
        return cache.get(key);
    }

    @Override
    public void put(final K key, final V value) {
        cache.put(key, value);
    }
}
