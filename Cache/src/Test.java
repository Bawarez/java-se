import by.epamlab.cache.Cache;
import by.epamlab.cache.LFUCache;
import by.epamlab.cache.LRUCache;

import java.util.stream.Stream;

/**
 * Uses CheckSumCalculator instance to test cache
 */
public final class Test {
    private static final int CACHE_SIZE = 3;
    private static final int THREADS_NUMBER = 4;
    private static String[] queryStrings = {"first", "second", "third", "fourth"};
    private static Integer[] querySequence = {0, 1, 2, 3, 1, 0, 1, 3, 2, 0, 1};

    private Test() { }

    /**
     * Runs simple test
     * @param args command string parameters
     * @throws InterruptedException if you aren't lucky
     */
    public static void main(final String[] args) throws InterruptedException {
        System.out.println("Running tests with cache size = " + CACHE_SIZE);

        test(new LRUCache<>(CACHE_SIZE), "Main thread LRU");
        System.out.println();

        test(new LFUCache<>(CACHE_SIZE), "Main thread LFU");
        System.out.println();

        runThreads(new LRUCache<>(CACHE_SIZE), "LRU");

        Thread.sleep(1000);
        System.out.println();

        runThreads(new LFUCache<>(CACHE_SIZE), "LFU");
    }

    private static void runThreads(final Cache<String, Integer> cache, final String infoSuffix) {
        for (int i = 1; i <= THREADS_NUMBER; i++) {
            final int threadNumber = i;
            new Thread(() -> test(cache, "Thread " + threadNumber + " " + infoSuffix)).start();
        }
    }

    private static void test(final Cache<String, Integer> cache, final String info) {
        CheckSumCalculator calc = new CheckSumCalculator(cache, info);
        System.out.println(info + " ran");
        Stream.of(querySequence)
                .forEach(i -> calc.printCheckSum(queryStrings[i]));
        System.out.println(info + " finished");
    }

    private static class CheckSumCalculator {
        private String testInfo;
        private Cache<String, Integer> cache;

        CheckSumCalculator(final Cache<String, Integer> cache, final String testInfo) {
            this.cache = cache;
            this.testInfo = " (" + testInfo + ")";
        }

        void printCheckSum(final String str) {
            String messageTail =  " result for [" + str + "]: ";

            Integer result = cache.get(str);
            if (result == null) {
                result = calculate(str);
                cache.put(str, result);
                System.out.println("Calculated" + messageTail + result + testInfo);
            } else {
                System.out.println("Cached" + messageTail + result + testInfo);
            }
        }

        private Integer calculate(final String str) {
            int result = 0;
            int i = 1;
            for (char ch : str.toCharArray()) {
                result += ch * i++;
            }
            return result;
        }
    }
}
